# openbsd-geoserver

Ansible playbook to install geoserver and basic openlayers site on OpenBSD

Edit ``hosts.yml`` to set variables and start with ``ansible-playbook geoserver.yml``
